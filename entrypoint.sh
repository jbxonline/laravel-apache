#!/bin/bash

echo "Starting PHP"
php-fpm -y /usr/local/etc/php-fpm.conf -D

echo "Starting Apache"
httpd-foreground

echo "Go build something amazing!"
